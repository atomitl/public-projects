package com.vagam.updater.local;

import com.vagam.updater.ProgressMonitor;
import com.vagam.updater.remote.AppPackage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.*;

/**
 * Created by atomi on 7/4/2018.
 */
public class FileHandler {

    private static final Logger logger = LogManager.getLogger();

    private AppPackage appPackage;
    private ProgressMonitor progressMonitor;

    public FileHandler(AppPackage app, ProgressMonitor monitor){
        this.appPackage = app;
        this.progressMonitor = monitor;
    }

    public File writeAppPackageFile(String appPackageFileName){
        logger.info("Downloading update file...");
        int i = 0;
        try{
            progressMonitor.setIndeterminateProgress(false);
            //Casting long to int, if file size is > 2047 MB it will overflow
            progressMonitor.setMaxProgress((int)(appPackage.getPackageSize()/1024)+1);
            File packageFile = new File(appPackageFileName);
            FileOutputStream fos = new FileOutputStream(packageFile);
            byte[] read_buf = new byte[1024];
            int read_len = 0;
            while ((read_len = appPackage.getInputStream().read(read_buf)) > 0) {
                progressMonitor.setStatusMessage("Descargando Actualizacion...");
                progressMonitor.setProgress(i);
                fos.write(read_buf, 0, read_len);
                i++;
            }
            appPackage.getInputStream().close();
            fos.close();
            return packageFile;
        } catch (FileNotFoundException e) {
            logger.error(e);
            progressMonitor.setStatusMessage("Error guardando el archivo de actualizacion...");

            System.exit(1);
        } catch (IOException e) {
            logger.error(e);
            progressMonitor.setStatusMessage("Error descargando el archivo de actualizacion...");
            System.exit(1);
        }
        return null;
    }
}
