package com.vagam.updater;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;

/**
 * Created by atomi on 7/4/2018.
 */
public class SplashScreen implements ProgressMonitor{

    private JFrame splash;
    private JProgressBar progressBar;
    private String appname;

    private static final Logger logger = LogManager.getLogger();

     SplashScreen(final String splashScreenImageFilename, final String appname){

         javax.swing.SwingUtilities.invokeLater(new Thread() {
             public void run() {
                 logger.info("Loading splashscreen...");
                 splash = new JFrame();
                 splash.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                 splash.setSize(400, 290);
                 splash.setUndecorated(true);
                 splash.setLocationRelativeTo(null);
                 JLabel lsplash = new JLabel(new ImageIcon(splashScreenImageFilename));
                 splash.setLayout(new BorderLayout());
                 splash.add(lsplash, BorderLayout.NORTH);
                 JPanel sur = new JPanel(new BorderLayout());
                 progressBar = new JProgressBar();
                 progressBar.setIndeterminate(true);
                 progressBar.setStringPainted(true);
                 progressBar.setString("Inicializando...");
                 sur.add(progressBar, BorderLayout.CENTER);
                 splash.add(sur, BorderLayout.SOUTH);
                 splash.setVisible(true);
             }
         });
    }

    public void setStatusMessage(final String text){
        javax.swing.SwingUtilities.invokeLater(new Thread() {
            public void run(){
                progressBar.setStringPainted(true);
                progressBar.setString(text);
            }
        });
    }

    public void setMaxProgress(final int maxProgress){
        javax.swing.SwingUtilities.invokeLater(new Thread() {
            public void run(){
                progressBar.setMaximum(maxProgress);
                progressBar.setIndeterminate(false);
            }
        });
    }

    public void setProgress(final int progress){
        javax.swing.SwingUtilities.invokeLater(new Thread() {
            public void run(){
                progressBar.setValue(progress);
            }
        });
    }

    public void setIndeterminateProgress(final boolean value){
        javax.swing.SwingUtilities.invokeLater(new Thread() {
            public void run(){
                progressBar.setIndeterminate(value);
            }
        });
    }
}
