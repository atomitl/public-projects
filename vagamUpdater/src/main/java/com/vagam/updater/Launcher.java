package com.vagam.updater;

import javax.swing.*;
import java.awt.*;
import java.util.Properties;
import java.io.*;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.vagam.updater.local.FileHandler;
import com.vagam.updater.remote.AppPackage;
import com.vagam.updater.remote.RemoteRepositoryConnector;
import com.vagam.updater.remote.RemoteRepositoryConnectorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by atomi on 7/4/2018.
 */
public class Launcher
{
    private static final Logger logger = LogManager.getLogger();
    private static final String configFileName = "updaterConfig.properties";
    public static final String defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static void main(String[] args) {
        Launcher launcher = new Launcher();
        launcher.start();
    }

    private SplashScreen splash;
    private String splashScreenImageFilename , appName, currentUpdateModifiedDate, temporaryDownloadPackageName;
    private Properties properties;

    Launcher(){
        loadConfigProperties();
    }

    public void start(){
        logger.info("*** Updater :: Appname: " + appName + " ***");
        splash = new SplashScreen(splashScreenImageFilename, appName.toUpperCase());
        RemoteRepositoryConnector repository = RemoteRepositoryConnectorFactory.getRemoteRepositoryConnector(appName, splash);
        AppPackage update = repository.getNewUpdateIfAvailable(currentUpdateModifiedDate);
        if(update != null){
            FileHandler fileHandler = new FileHandler(update, splash);
            fileHandler.writeAppPackageFile(temporaryDownloadPackageName);
            properties.setProperty("currentUpdateModifiedDate", update.getUpdateDate());
            saveConfigProperties();
        }
        logger.info("Exiting the updater app...");
        System.exit(0);
    }

    private void loadConfigProperties(){
        logger.info("Loading Config Properties...");
        properties = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(configFileName);
            properties.load(input);
            splashScreenImageFilename = properties.getProperty("splashScreenImageFilename");
            appName = properties.getProperty("appName");
            currentUpdateModifiedDate = properties.getProperty("currentUpdateModifiedDate");
            temporaryDownloadPackageName = properties.getProperty("temporaryDownloadPackageName");
        } catch (IOException ex) {
            logger.error(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        }
    }

    private void saveConfigProperties(){
        logger.info("Saving Config Properties...");
        OutputStream output = null;
        try {
            output = new FileOutputStream(configFileName);
            properties.store(output, null);
        } catch (IOException ex) {
            logger.error(ex);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        }
    }
}
