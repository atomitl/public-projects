package com.vagam.updater;

/**
 * Created by atomi on 7/5/2018.
 */
public interface ProgressMonitor {

    public void setStatusMessage(String message);
    public void setMaxProgress(int value);
    public void setProgress(int value);
    public void setIndeterminateProgress(boolean value);
}
