package com.vagam.updater.remote;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by atomi on 7/5/2018.
 */
public class AppPackage {
    private InputStream inputStream;
    private long packageSize;
    private String filename;
    private String updateDate;

    AppPackage(String filename, InputStream is, long size, Date date){
        this.filename = filename;
        this.inputStream = is;
        this.packageSize = size;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        updateDate = df.format(date);
    }

    public String getFilename() {
        return filename;
    }

    public long getPackageSize() {
        return packageSize;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getUpdateDate() {
        return updateDate;
    }
}
