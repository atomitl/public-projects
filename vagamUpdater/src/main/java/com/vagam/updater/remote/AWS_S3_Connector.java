package com.vagam.updater.remote;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.vagam.updater.Launcher;
import com.vagam.updater.ProgressMonitor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

/**
 * Created by atomi on 7/4/2018.
 */
public class AWS_S3_Connector implements RemoteRepositoryConnector {

    private static final Logger logger = LogManager.getLogger();

    private String bucketName, filePrefix;
    private Date currentUpdateModifiedDate;
    private ProgressMonitor progressMonitor;

    AWS_S3_Connector(String filePrefix, ProgressMonitor monitor){
        this.bucketName = "update.vagam.mx";
        this.filePrefix = filePrefix;
        this.progressMonitor = monitor;
    }

    /**
     * Searches through the S3 Bucket for an app package with a modified date newer than the date of the
     * current version. If it exists, it returns the latest app package, null otherwise.
     *
     * @param   currentUpdateModifiedDateString The date of the current update with format "yyyy-MM-dd"
     * @return      the zip file containing the updated app, null if there isn't a new update.
     */
    public AppPackage getNewUpdateIfAvailable(String currentUpdateModifiedDateString) {
        progressMonitor.setIndeterminateProgress(true);
        progressMonitor.setStatusMessage("Buscando actualizaciones...");
        DateFormat df = new SimpleDateFormat(Launcher.defaultDateFormat);
        try{
            this.currentUpdateModifiedDate = df.parse(currentUpdateModifiedDateString);
            logger.debug("currentUpdateModifiedDateString = " + currentUpdateModifiedDate);
        }catch (ParseException e){
            logger.error(e);
        }
        logger.info("Retrieving S3 Bucket list");
        try {
            final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
            ListObjectsV2Result result = s3.listObjectsV2(bucketName, filePrefix);
            List<S3ObjectSummary> objects = result.getObjectSummaries();

            for (S3ObjectSummary os : objects) {
                logger.debug("  * " + os.getKey() + " - modified: " + os.getLastModified());
                if (os.getLastModified().after(currentUpdateModifiedDate)) {
                    logger.info("Found a new update! - " + os.getKey() + " / " + os.getLastModified());
                    S3Object object = s3.getObject(bucketName, os.getKey());
                    return new AppPackage(os.getKey(), object.getObjectContent(), os.getSize(), os.getLastModified());
                }
            }
            logger.info("A new update was not found.");
        }catch (Exception e){
            logger.error(e);
            progressMonitor.setStatusMessage("ERROR: No fue posible conectarse al servidor de actualizaciones");
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e1) {
                logger.error(e1);
                System.exit(1);
            }
            System.exit(1);
        }
        return null;
    }
}
