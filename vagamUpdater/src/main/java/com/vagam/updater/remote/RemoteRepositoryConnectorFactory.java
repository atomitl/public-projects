package com.vagam.updater.remote;

import com.vagam.updater.ProgressMonitor;

import java.io.File;

/**
 * Created by atomi on 7/4/2018.
 */
public class RemoteRepositoryConnectorFactory {
    public static RemoteRepositoryConnector getRemoteRepositoryConnector(String filePrefix, ProgressMonitor monitor){
        return new AWS_S3_Connector(filePrefix, monitor);
    }
}
