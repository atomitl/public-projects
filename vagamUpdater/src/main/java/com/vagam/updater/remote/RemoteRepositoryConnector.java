package com.vagam.updater.remote;

import java.io.InputStream;

/**
 * Created by atomi on 7/4/2018.
 */
public interface RemoteRepositoryConnector {

    AppPackage getNewUpdateIfAvailable(String currentUpdateModifiedDateString);

}