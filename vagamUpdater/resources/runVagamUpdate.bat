@echo off
java -Dlog4j.configurationFile=log4j2.properties -jar vagam-updater.jar
if exist newdownload.zip (
    7z a backup.7z * -r -y
    7z x newdownload.zip -y
    del newdownload.zip
)
start javaw -jar Vagam.jar
exit
